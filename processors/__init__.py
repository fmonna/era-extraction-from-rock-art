import importlib
import pkgutil
import numpy


def get_processors():

    def iter_namespace(ns_pkg):
        return pkgutil.iter_modules(__path__, __name__ + ".")

    def load_processors():
        for _, name, _ in iter_namespace(__name__):
            try:
                processor = getattr(importlib.import_module(name), 'Processor')
            except Exception as err:
                continue
            processors_list.append(processor)
    processors_list = []

    load_processors()
    return sorted(processors_list, key=lambda x: x.order)

