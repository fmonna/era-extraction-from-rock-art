import importlib
import pkgutil
import cv2

class ColorInterpreter:
    name = 'RGB'
    order = 0

    def interpret(self, matrix):
        """
        return RGB matrix from image.
        """
        return matrix

def get_color_interpreters():

    def iter_namespace(ns_pkg):
        return pkgutil.iter_modules(__path__, __name__ + ".")

    def load_color_interpreters():
        for _, name, _ in iter_namespace(__name__):
            try:
                color_interpreter = getattr(importlib.import_module(name), 'ColorInterpreter')
            except:
                continue
            color_interpreters_list.append(color_interpreter)
    color_interpreters_list = []

    load_color_interpreters()
    color_interpreters_list.append(ColorInterpreter)
    return sorted(color_interpreters_list, key=lambda x: x.order)

