import cv2

class ColorInterpreter:
    name = 'HLS'
    order = 60

    def interpret(self, matrix):
        """
        return HLS matrix from image.
        """
        converted = cv2.cvtColor(matrix, cv2.COLOR_RGB2HLS)
        return converted
