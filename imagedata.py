import cv2
from qt import QtGui
from color_interpreters import get_color_interpreters
from processors import get_processors
from processors.utils import matrix_stats


color_interpreters_dict = {ci.name: ci() for ci in get_color_interpreters()}
processors_dict = {proc.name: proc() for proc in get_processors()}


class ColorSpaceBundle:
    """
    Pack QImage for composite and channels
    """
    def __init__(self, code, data):
        self.buffer = data
        self.raw_channels = cv2.split(self.buffer)
        self.code = code
        self.composite = None
        self.channels = []
        self.set_data()

    def set_data(self):
        width, height, depth = self.buffer.shape
        self.composite = QtGui.QImage(self.buffer, height, width, depth * height, QtGui.QImage.Format_RGB888)
        for channel in self.raw_channels:
            self.channels.append(QtGui.QImage(channel, height, width, height, QtGui.QImage.Format_Grayscale8))

    def get_blurred(self, blur=5):
        return cv2.blur(self.buffer, (blur, blur))

    def get_stats(self):
        print(matrix_stats(self.buffer))

class ImageData:
    def __init__(self, image=None):
        self.raw = None
        self.reference = None
        self.processed = {}
        self.image_path = None

    def load(self, image_path, progress_bar=None):
        self.reset()
        self.image_path = image_path
        self.raw = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
        if progress_bar:
            variations = len(processors_dict) * len(color_interpreters_dict)
            progress_bar.setMaximum(variations)
            step = 0

        interpreted = list(color_interpreters_dict.values())[0].interpret(self.raw)
        self.reference = ColorSpaceBundle(list(color_interpreters_dict.keys())[0], interpreted)
        for processor in processors_dict:
            self.processed[processor] = {}
            processed = processors_dict[processor].process(self.raw)
            for interpreter in color_interpreters_dict:
                interpreted = color_interpreters_dict[interpreter].interpret(processed)
                color_space_bundle = ColorSpaceBundle(interpreter, interpreted)
                self.processed[processor][interpreter] = color_space_bundle
                if progress_bar:
                    step += 1
                    progress_bar.setValue(step)

    def reset(self):
        self.raw = None
        self.image_path = None
        self.processed = {}

    def update(self, mask=None, keep=0, progress_bar=None):
        self.processed = {}
        if progress_bar:
            variations = len(processors_dict) * len(color_interpreters_dict)
            progress_bar.setMaximum(variations)
            step = 0
        for processor in processors_dict:
            self.processed[processor] = {}
            processed = processors_dict[processor].process(self.raw, mask=mask, keep=keep)
            for interpreter in color_interpreters_dict:
                interpreted = color_interpreters_dict[interpreter].interpret(processed)
                color_space_bundle = ColorSpaceBundle(interpreter, interpreted)
                self.processed[processor][interpreter] = color_space_bundle
                if progress_bar:
                    step += 1
                    progress_bar.setValue(step)

    def get_bundles(self, with_reference=True):
        bundles = [i for p in self.processed.values() for i in p.values()]
        if with_reference:
            bundles.insert(0, self.reference)
        return bundles
