import numpy
from sklearn.decomposition import FastICA
from .utils import reshape_scale


class Processor:
    name = 'FastICA'
    order = 40

    def process(self, matrix, mask=None, keep=0):
        w, h, c = matrix.shape
        matrix = matrix.reshape(w * h, -1)
        if mask is None:
            subset = matrix
        else:
            mask = mask.reshape(w * h)
            subset = matrix[mask]

        transformer = FastICA(n_components=3)
        transformer.fit(subset)
        X_transformed = transformer.transform(matrix)
        sub = transformer.transform(subset)
        
        return reshape_scale(X_transformed, sub, w, h, keep)

