import cv2

class ColorInterpreter:
    name = 'LUV'
    order = 30

    def interpret(self, matrix):
        """
        return LUV matrix from image.
        """
        converted = cv2.cvtColor(matrix, cv2.COLOR_RGB2LUV)
        return converted
