import cv2

class ColorInterpreter:
    name = 'YCrCb'
    order = 40

    def interpret(self, matrix):
        """
        return YCrCb matrix from image.
        """
        converted = cv2.cvtColor(matrix, cv2.COLOR_RGB2YCrCb)
        return converted
