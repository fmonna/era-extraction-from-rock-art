import sys
import time
import numpy
import os
from widgets import *
from imagedata import ImageData
from qt import QtGui, QtWidgets, QtCore, Slot, Signal
import qrc_resources
from processors import get_processors

import cv2
from color_interpreters import get_color_interpreters
from scipy.stats import ttest_ind
from cleanlab.classification import LearningWithNoisyLabels
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler  
from sklearn.decomposition import PCA
from sklearn.decomposition import FastICA
from sklearn.neighbors import KNeighborsClassifier
import qimage2ndarray
XWin = None
YWin = None

DEFAULT_BLUR = 5
DEFAULT_PCA_SIZE = 97
DEFAULT_N_SIZE = 30
DEFAULT_GAIN = 1
DEFAULT_PEN_SIZE = 10

root_folder = os.path.dirname(__file__)
doc_folder = os.path.join(root_folder, 'resources', 'doc')
about_content = os.path.join(doc_folder, 'about.txt')

left_to_right = True

process_methods = {proc.name: proc() for proc in get_processors()}
color_interpreters = {col.name: col() for col in get_color_interpreters()}


def convert_QImage_to_mask(image):
    image = qimage2ndarray.rgb_view(image)
    height, width, _ = image.shape
    array = image.reshape(-1, 3).min(axis=1)
    array = numpy.where(array == 255, numpy.full(array.shape, False), numpy.full(array.shape, True))
    array = array.reshape(height, width)
    if not array.any():
        return None
    return array


def empty_layout(layout):
    while layout.count():
        child = layout.takeAt(0)
        if child.widget():
            layout.removeWidget(child.widget())
            child.widget().setParent(None)




def active_tab_title(tablayout):
    current_index = tablayout.currentIndex()
    if current_index > -1:
        return tablayout.tabText(current_index)
    else:
        return None


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.graphics_scene = None
        self.setWindowTitle("ERA — Extraction of Rock Art")
        self.setWindowIcon(QtGui.QIcon(':/icon.png'))
        self.data = ImageData()
        self.central_widget = QtWidgets.QWidget()               
        self.setCentralWidget(self.central_widget)
        lay = QtWidgets.QVBoxLayout(self.central_widget)
        label_image = QtWidgets.QLabel(self)
        pixmap0 = QtGui.QPixmap(':/icon.png')
        label_image.setPixmap(pixmap0)
        label_image.setAlignment(QtCore.Qt.AlignCenter)
        lay.addWidget(label_image)
        self.setup_ui()

    def setup_ui(self):

        # actions
        
        open_action = QtWidgets.QAction(QtGui.QIcon(':/open.svg'), self.tr('Open'), self)  
        open_action.setShortcut(QtGui.QKeySequence.Open)
        open_action.setStatusTip(self.tr("Open an image"))
        open_action.triggered.connect(self.load_data)
        
        exit_action = QtWidgets.QAction(QtGui.QIcon(':/exit.svg'), self.tr("Exit"), self)
        exit_action.setShortcut(QtGui.QKeySequence.Quit)
        exit_action.setStatusTip(self.tr("Close application"))
        exit_action.triggered.connect(self.close)

        snap_action = QtWidgets.QAction(QtGui.QIcon(':/snap.svg'), self.tr('Snapshot'), self)  
        snap_action.setShortcut(QtGui.QKeySequence.Save)
        snap_action.setStatusTip(self.tr("Capture the main window"))
        snap_action.triggered.connect(self.snapshot)
        
        
        info_action = QtWidgets.QAction(self.tr('Help'), self)  
        snap_action.setShortcut(QtGui.QKeySequence.HelpContents)
        info_action.setStatusTip(self.tr("General workflow described"))
        info_action.triggered.connect(self.display_help)
        
        about_action = QtWidgets.QAction(self.tr('About...'), self)  
        about_action.setStatusTip(self.tr("Contributions"))
        about_action.triggered.connect(self.display_about)

        # menu
        self.menu = self.menuBar()
        self.file_menu = self.menu.addMenu(self.tr("File"))
        self.snap_menu = self.menu.addMenu(self.tr('Capture'))
        self.info_menu = self.menu.addMenu(self.tr('?'))

        self.file_menu.addAction(open_action)
        self.file_menu.addAction(exit_action)

        self.snap_menu.addAction(snap_action)

        self.info_menu.addAction(info_action)
        self.info_menu.addAction(about_action)

        # tool bar

        self.toolbar = self.addToolBar( self.tr("Standard ToolBar") )
        self.toolbar.addAction(open_action)
        self.toolbar.addAction(snap_action)
        self.toolbar.addAction(exit_action)

        # status bar
        self.status = self.statusBar()
        self.status.showMessage(self.tr("Ready to rock"))
        self.progress_bar = QtWidgets.QProgressBar()
        self.status.addPermanentWidget(self.progress_bar)

        ### layout
        ## global tab structure with two tabs
        ## first tab contains supervision utilities
        ## second tab contains result and refinement parameters

        self.tab_widget = QtWidgets.QTabWidget()
        #self.setCentralWidget(self.tab_widget)


        ## first tab: decorrelation and supervision (sv)
        decorrelation_and_supervision_tab_widget = QtWidgets.QWidget(self.tab_widget)
        self.tab_widget.addTab(decorrelation_and_supervision_tab_widget, self.tr('Supervision'))
        self.tab_widget.setCurrentIndex(0)
        self.tab_widget.currentChanged.connect(self.reset_ml_method)

        decorrelation_and_supervision_tab_layout = QtWidgets.QHBoxLayout(decorrelation_and_supervision_tab_widget)
        decorrelation_and_supervision_tab_splitter = QtWidgets.QSplitter(decorrelation_and_supervision_tab_widget)
        decorrelation_and_supervision_tab_layout.addWidget(decorrelation_and_supervision_tab_splitter)
        #if not left_to_right:
        #    decorrelation_and_supervision_tab_layout.setDirection(QtWidgets.QBoxLayout.RightToLeft)
        decorrelation_and_supervision_tab_widget.setLayout(decorrelation_and_supervision_tab_layout)

        decorrelation_widget = QtWidgets.QWidget(decorrelation_and_supervision_tab_widget)
        self.decorrelation_layout = QtWidgets.QVBoxLayout(decorrelation_widget)
        decorrelation_widget.setLayout(self.decorrelation_layout)
        decorrelation_and_supervision_tab_splitter.addWidget(decorrelation_widget)

        supervision_widget = QtWidgets.QWidget(decorrelation_and_supervision_tab_widget)
        supervision_layout = QtWidgets.QVBoxLayout(supervision_widget)
        supervision_widget.setLayout(supervision_layout)
        decorrelation_and_supervision_tab_splitter.addWidget(supervision_widget)
        decorrelation_and_supervision_tab_splitter.setStretchFactor(2,5)

        # drawing
        self.image_widget = ImageWidget(parent=supervision_widget)
        supervision_layout.addWidget(self.image_widget, stretch=1)


        # channels
        channel_widget = QtWidgets.QWidget(supervision_widget)
        supervision_layout.addWidget(channel_widget, stretch=0)

        outer_channel_layout = QtWidgets.QHBoxLayout()
        self.channel_layout = QtWidgets.QHBoxLayout()
        outer_channel_layout.addStretch()
        inner_channel_widget = QtWidgets.QWidget(supervision_widget)
        inner_channel_widget.setLayout(self.channel_layout)
        outer_channel_layout.addWidget(inner_channel_widget, stretch=0)
        outer_channel_layout.addStretch()
        supervision_tools_widget = QtWidgets.QWidget()
        supervision_tools_layout = QtWidgets.QFormLayout()
        
        

        include_draw_method_radio = QtWidgets.QRadioButton('include')
        exclude_draw_method_radio = QtWidgets.QRadioButton('exclude')
        self.include_pen_size_spinbox = QtWidgets.QSpinBox()
        self.include_pen_size_spinbox.valueChanged.connect(self.set_include_pen_width)
        self.exclude_pen_size_spinbox = QtWidgets.QSpinBox()
        self.exclude_pen_size_spinbox.valueChanged.connect(self.set_exclude_pen_width)
        supervision_tools_layout.addRow(include_draw_method_radio, self.include_pen_size_spinbox)
        supervision_tools_layout.addRow(exclude_draw_method_radio, self.exclude_pen_size_spinbox)
        undo_stroke_button = QtWidgets.QPushButton('Undo')
        undo_stroke_button.setToolTip(self.tr("Undo last stroke for current mode"))
        undo_stroke_button.clicked.connect(self.undo_stroke)
        redo_stroke_button = QtWidgets.QPushButton('Redo')
        redo_stroke_button.setToolTip(self.tr("Redo last stroke for current mode"))
        redo_stroke_button.clicked.connect(self.redo_stroke)
        supervision_tools_layout.addRow(undo_stroke_button, redo_stroke_button)
        reset_supervision_button = QtWidgets.QPushButton(self.tr('Reset'))
        reset_supervision_button.setToolTip(self.tr("Reset current mode"))
        reset_supervision_button.clicked.connect(self.reset_strokes)
        resetall_supervision_button = QtWidgets.QPushButton(self.tr('Reset All'))
        resetall_supervision_button.setToolTip(self.tr("Reset all modes"))
        resetall_supervision_button.clicked.connect(self.reset_all_strokes)
        continue_push_button = QtWidgets.QPushButton(self.tr('Continue'))
        continue_push_button.clicked.connect(self.process)
        supervision_tools_layout.addRow(reset_supervision_button, resetall_supervision_button)
        supervision_tools_layout.addRow(continue_push_button)
        supervision_tools_widget.setLayout(supervision_tools_layout)
        outer_channel_layout.addWidget(supervision_tools_widget, stretch=0)
        channel_widget.setLayout(outer_channel_layout)

        # colorspaces
        #colorspace_container_widget = QtWidgets.QWidget()
        #self.colorspace_container_layout = QtWidgets.QVBoxLayout()
        #colorspace_container_widget.setLayout(self.colorspace_container_layout)
        self.colorspace_container_widget = QtWidgets.QTabWidget()
        self.colorspace_container_widget.setTabPosition(QtWidgets.QTabWidget.West)
        self.colorspace_container_widget.currentChanged.connect(self.reparenting_ref_colorbundle)
        self.ref_thumbnail = None


        self.decorrelation_layout.addWidget(self.colorspace_container_widget, stretch=2)
        
        ## decorrelation
        self.process_widget = QtWidgets.QWidget(decorrelation_widget)

        process_form_widget = QtWidgets.QWidget()
        process_form_layout = QtWidgets.QFormLayout()
        process_form_widget.setLayout(process_form_layout)
        self.decorrelation_layout.addWidget(self.process_widget, stretch=0)

        select_draw_method_radio = QtWidgets.QRadioButton('select')
        self.select_pen_size_spinbox = QtWidgets.QSpinBox()
        self.select_pen_size_spinbox.valueChanged.connect(self.set_select_pen_width)
        process_form_layout.addRow(self.tr('Stroke Width'), self.select_pen_size_spinbox)
        self.contrast_boost_spinbox = QtWidgets.QSpinBox()
        self.contrast_boost_spinbox.setRange(0, 20)
        process_form_layout.addRow(self.tr('Contrast Boost'), self.contrast_boost_spinbox)
        process_push_button = QtWidgets.QPushButton(self.tr('Decorrelation Refinement'))
        process_push_button.clicked.connect(self.decorrelate)


        process_box = QtWidgets.QVBoxLayout()
        process_box.addWidget(select_draw_method_radio)
        process_box.addWidget(process_form_widget)
        process_box.addWidget(process_push_button)

        self.process_widget.setLayout(process_box)

        ## second tab: result and refinement (rr)
        drawing_tab_widget = QtWidgets.QWidget(self.tab_widget)
        self.tab_widget.addTab(drawing_tab_widget, self.tr('Drawing'))
        self.tab_widget.setTabEnabled(1, False)

        drawing_tab_layout = QtWidgets.QHBoxLayout(drawing_tab_widget)
        drawing_tab_widget.setLayout(drawing_tab_layout)

        # left part
        drawing_widget = QtWidgets.QWidget(drawing_tab_widget)
        drawing_tab_layout.addWidget(drawing_widget)
        drawing_grid = QtWidgets.QGridLayout(drawing_widget)
        drawing_widget.setLayout(drawing_grid)

        self.results = {'with confident learning': {},
                        'without confident learning': {},
                        'white background': {},
                        'black foreground': {},
                        }
        self.results['with confident learning'].setdefault('view', ResultWidget(label='With confident learning'))
        self.results['without confident learning'].setdefault('view', ResultWidget(label='Without confident learning'))
        self.results['white background'].setdefault('view', ResultWidget(label='White background'))
        self.results['black foreground'].setdefault('view', ResultWidget(label='Black foreground'))

        result_coords = [(i//2, i%2) for i in range(len(self.results))]
        for result, coords in zip(self.results.values(), result_coords):
            drawing_grid.addWidget(result['view'], *coords)

        # right part
        refine_widget = QtWidgets.QWidget(drawing_tab_widget)
        refine_layout = QtWidgets.QFormLayout()
        refine_widget.setLayout(refine_layout)

        drawing_tab_layout.addWidget(refine_widget)
        #refine_layout = QtWidgets.QVBoxLayout(refine_widget)
        refine_widget.setLayout(refine_layout)
        self.blursize = QtWidgets.QSpinBox(self)
        self.blursize.setSingleStep(1)
        self.blursize.setRange(1,50)
        self.blursize.setValue(DEFAULT_BLUR)
        refine_layout.addRow(self.tr('Blur Radius'), self.blursize)

        self.PCAsize = QtWidgets.QSpinBox(self)
        self.PCAsize.setSingleStep(1)
        self.PCAsize.setRange(75,100)
        self.PCAsize.setValue(DEFAULT_PCA_SIZE)
        refine_layout.addRow(self.tr('PCA var. explained:'), self.PCAsize)
        #self.PCAsize.valueChanged.connect(self.valuechange4)

        self.Nsize = QtWidgets.QSpinBox(self)
        self.Nsize.setSingleStep(1)
        self.Nsize.setRange(10,102)
        self.Nsize.setValue(DEFAULT_N_SIZE)
        refine_layout.addRow(self.tr('n best channels:'), self.Nsize)
        #self.Nsize.valueChanged.connect(self.valuechange5)
        #
        self.prediction_button_lr = QtWidgets.QRadioButton('LR')
        prediction_button_svm = QtWidgets.QRadioButton('SVM')
        prediction_button_knn = QtWidgets.QRadioButton('K-NN')
        self.prediction_button_lr.setChecked(True)

        refine_layout.addRow('Prediction Method:', self.prediction_button_lr)
        refine_layout.addRow('', prediction_button_svm)
        refine_layout.addRow('', prediction_button_knn)

        btnReprocess = QtWidgets.QPushButton(self.tr("Re-process"), self)
        btnReprocess.setFixedWidth(100)
        btnReprocess.setToolTip(self.tr("Reprocess <i>Calculate</i>"))      
        btnReprocess.clicked.connect(self.process)
        #
        btnSave = QtWidgets.QPushButton(self.tr("Save"), self)
        btnSave.setFixedWidth(100)
        btnSave.setToolTip(self.tr("Save the checked images"))  
        refine_layout.addRow(btnReprocess, btnSave)
        btnSave.clicked.connect(self.saveImage)
        #       

        ### selection groups
        self.draw_mode_selection_group = QtWidgets.QButtonGroup()
        self.draw_mode_selection_group.buttonClicked.connect(self.set_pen_mode)
        self.draw_mode_selection_group.addButton(select_draw_method_radio)
        self.draw_mode_selection_group.addButton(include_draw_method_radio)
        self.draw_mode_selection_group.addButton(exclude_draw_method_radio)
        self.channel_selection_group = QtWidgets.QButtonGroup(channel_widget)
        self.channel_selection_group.buttonClicked.connect(self.refresh_canvas)
        self.colorspace_selection_group = QtWidgets.QButtonGroup()
        self.colorspace_selection_group.buttonClicked.connect(self.refresh_channels)
        self.prediction_method_group = QtWidgets.QButtonGroup()
        self.prediction_method_group.addButton(self.prediction_button_lr)
        self.prediction_method_group.addButton(prediction_button_svm)
        self.prediction_method_group.addButton(prediction_button_knn)
        self.current_colorspace = None


        ## second tab

    def reset_ml_method(self):
        self.prediction_button_lr.setChecked(True)

    def reset_prediction_parameters(self):
        self.prediction_button_lr.setChecked(True)
        self.Nsize.setValue(DEFAULT_N_SIZE)
        self.PCAsize.setValue(DEFAULT_PCA_SIZE)
        self.blursize.setValue(DEFAULT_BLUR)
        for result in self.results.values():
            result['view'].scene.removeItem(result['view'].background_pixmap)
            result['view'].background_pixmap = None
            result['view'].uncheck()
        self.tab_widget.setTabEnabled(1, False)
        self.tab_widget.setCurrentIndex(0)

    def reset_supervision_parameters(self):
        self.draw_mode_selection_group.buttons()[0].click()
        if self.colorspace_selection_group.buttons():
            self.colorspace_selection_group.buttons()[0].click()
        self.set_pen_mode()
        self.select_pen_size_spinbox.setValue(DEFAULT_PEN_SIZE)
        self.include_pen_size_spinbox.setValue(DEFAULT_PEN_SIZE)
        self.exclude_pen_size_spinbox.setValue(DEFAULT_PEN_SIZE)
        for mode in ['select', 'include', 'exclude']:
            self.set_pen_width(mode, DEFAULT_PEN_SIZE)
        self.contrast_boost_spinbox.setValue(DEFAULT_GAIN)

    def process(self):
        case_mask = convert_QImage_to_mask(self.graphics_scene.render_layer('include'))
        not_case_mask = convert_QImage_to_mask(self.graphics_scene.render_layer('exclude'))
        if case_mask is None or not_case_mask is None:
            message = self.tr("""You must supervise first!

Choose the size of the pen and select the option include / exclude
for painting the areas corresponding to the figure and the surrounding, respectively.""")
            self.modal_error(message)
            return
        prediction_method = self.prediction_method_group.checkedButton().text()
        self.status.showMessage(self.tr('Running prediction with {} method').format(prediction_method))
        self.progress_bar.setMaximum(10)
        height, width, c = self.data.raw.shape
        blurred_channels = [c.get_blurred(blur=self.blursize.value()) for c in self.data.get_bundles(with_reference=False)]


        color_case = numpy.column_stack([c[case_mask] for c in blurred_channels])
        color_notcase = numpy.column_stack([c[not_case_mask] for c in blurred_channels])

        select2 = [False] * color_case.shape[1]
        sel = []
        
        for i in range(color_case.shape[1]):

                stat, p = ttest_ind(color_case[:,i], color_notcase[:,i], equal_var = False)
                sel.append(abs(stat))


        L20prems = numpy.argsort(sel)[-self.Nsize.value():]
        select2 = numpy.array(select2)
        select2[L20prems] = True
        
        X_train = numpy.vstack((color_case[:,select2], color_notcase[:,select2]))
        Y = numpy.asarray([1] * color_case[:,select2].shape[0] + [0] * color_notcase[:,select2].shape[0])     
        y_train = numpy.asarray(Y, dtype=int)
        
        self.progress_bar.setValue(1)

        scaler = StandardScaler()
        scaler.fit(X_train)
        X_train_STD_0 = scaler.transform(X_train)

        PCA_size = self.PCAsize.value()
        if (PCA_size/100) == 1:
            variance_exp = 0.9999999
        else :
            variance_exp = PCA_size/100

        pca = PCA(variance_exp).fit(X_train_STD_0)
        
        X_train_STD = pca.transform(X_train_STD_0)

        
        if prediction_method == 'LR':
            
            clfwn = LearningWithNoisyLabels(clf=LogisticRegression(solver = 'lbfgs',max_iter=10000), n_jobs=1)     
            clfwn.fit(X_train_STD, y_train)
            
            clf = LogisticRegression(solver = 'lbfgs',max_iter=10000, n_jobs=1)
            clf.fit(X_train_STD, y_train)
        
        if prediction_method == 'K-NN':
            
            clfwn = LearningWithNoisyLabels(clf=KNeighborsClassifier(n_neighbors=11), n_jobs=1)
            clfwn.fit(X_train_STD, y_train)
  
            clf = KNeighborsClassifier(n_neighbors=11)
            clf.fit(X_train_STD, y_train)

        if prediction_method == 'SVM':
            scalerSVM = StandardScaler()
            scalerSVM.fit(X_train_STD)
            X_train_STD_SVM = scalerSVM.transform(X_train_STD)

            clfwn = LearningWithNoisyLabels(clf= SVC(probability = True), n_jobs=1)    
            clfwn.fit(X_train_STD_SVM, y_train)
  
            clf = SVC(probability = True)
            clf.fit(X_train_STD_SVM, y_train)
            
        
        seuil = 0.5
        colorscomplet = numpy.column_stack([c.reshape(height * width, -1) for c in blurred_channels])

        colors = scaler.transform(colorscomplet[:,select2])
        colors_STDcomplet = pca.transform(colors)
        
        if prediction_method == 'SVM':
            colors_STDcomplet = scalerSVM.transform(colors_STDcomplet)

        r = clfwn.predict(colors_STDcomplet)
        r2 = clf.predict(colors_STDcomplet)
        
        resultat = r.reshape((height, width))
        resultat2 = r2.reshape((height, width))
        
        result_clfwn = numpy.where(resultat> seuil, 0,255)
        result_clf = numpy.where(resultat2> seuil, 0,255)
        
        c1=numpy.where(resultat>seuil, self.data.raw[:,:,0],255)
        c2=numpy.where(resultat>seuil, self.data.raw[:,:,1],255)
        c3=numpy.where(resultat>seuil, self.data.raw[:,:,2],255)
        
        resultat_bw=numpy.dstack((c1,c2,c3))
        qImg  = QtGui.QImage(resultat_bw, width, height, 3 * width, QtGui.QImage.Format_RGB888)
        self.results['white background']['view'].set_background_pixmap(QtGui.QPixmap.fromImage(qImg))
        self.results['white background']['data'] = resultat_bw
        
        c1=numpy.where(resultat>seuil, 0, self.data.raw[:,:,0])
        c2=numpy.where(resultat>seuil, 0, self.data.raw[:,:,1])
        c3=numpy.where(resultat>seuil, 0, self.data.raw[:,:,2])
        
        resultat_color = numpy.dstack((c1,c2,c3))
        qImg  = QtGui.QImage(resultat_color, width, height, 3 * width, QtGui.QImage.Format_RGB888)
        self.results['black foreground']['view'].set_background_pixmap(QtGui.QPixmap.fromImage(qImg))
        self.results['black foreground']['data'] = resultat_color
        
        resultat_clfwn = result_clfwn.astype(numpy.uint8)
        qImg  = QtGui.QImage(resultat_clfwn, width, height, width, QtGui.QImage.Format_Grayscale8)
        self.results['with confident learning']['view'].set_background_pixmap(QtGui.QPixmap.fromImage(qImg))
        self.results['with confident learning']['data'] = resultat_clfwn
        
        resultat_clf = result_clf.astype(numpy.uint8)
        qImg  = QtGui.QImage(resultat_clf, width, height, width, QtGui.QImage.Format_Grayscale8)
        self.results['without confident learning']['view'].set_background_pixmap(QtGui.QPixmap.fromImage(qImg))
        self.results['without confident learning']['data'] = resultat_clf

        self.progress_bar.setValue(10)
        self.progress_bar.reset()
        self.status.clearMessage()
        self.tab_widget.setTabEnabled(1, True)
        self.tab_widget.setCurrentIndex(1)

    def saveImage(self):
        results_to_save = [key for key in self.results if self.results[key]['view'].is_selected_for_export()]
        if results_to_save:
            image_name = os.path.splitext(os.path.basename(self.data.image_path))[0]
            image_directory = os.path.dirname(self.data.image_path)
            directory = QtWidgets.QFileDialog.getExistingDirectory(self, 'Choose destination folder', image_directory, QtWidgets.QFileDialog.ShowDirsOnly | QtWidgets.QFileDialog.DontResolveSymlinks) 

            for key in results_to_save:

                result = self.results[key]
                image_path = os.path.join(directory, f"{image_name}-{key.replace(' ', '_')}.tif")

                cv2.imwrite(image_path, cv2.cvtColor(result['data'], cv2.COLOR_BGR2RGB))

                if key in ['without confident learning', 'with confident learning']:
                    contours, hierarchy = cv2.findContours(result['data'], cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                    with open(os.path.join(directory, f"{image_name}-{key.replace(' ', '_')}.svg"), "w") as f:
                        f.write(f'<svg xmlns="http://www.w3.org/2000/svg">')
                        for c in contours:
                            f.write('<path d="M')
                            for i in range(len(c)):
                                x, y = c[i][0]
                                f.write(f"{x} {y} ")
                            f.write('" style="stroke:white"/>')
                        f.write("</svg>")
        else:
            self.modal_error(self.tr('Please select image to save first'))


    def decorrelate(self):
        image = self.graphics_scene.render_layer('select')
        mask = convert_QImage_to_mask(image)
        self.data.update(mask=mask, keep=self.contrast_boost_spinbox.value(), progress_bar=self.progress_bar)
        self.refresh_tabbed_colorspace_picker()
        self.progress_bar.reset()
        self.status.clearMessage()

    def reparenting_ref_colorbundle(self):
        if self.colorspace_container_widget.currentWidget():
            self.colorspace_container_widget.currentWidget().widget().children()[0].addWidget(self.ref_thumbnail, 0, 0)
            method = self.colorspace_container_widget.tabText(self.colorspace_container_widget.currentIndex())
            for item in self.colorspace_selection_group.buttons():
                if item.text() == self.current_colorspace:
                    if item in self.colorspace_container_widget.currentWidget().widget().children():
                        item.click()

    def undo_stroke(self):
        self.image_widget.history_event('undo')

    def redo_stroke(self):
        self.image_widget.history_event('redo')

    def reset_strokes(self):
        self.graphics_scene.flush_history(length=0)

    def reset_all_strokes(self):
        self.graphics_scene.flush_all_history()

    def set_pen_mode(self):
        self.graphics_scene.set_pen_mode(self.draw_mode_selection_group.checkedButton().text())

    def set_select_pen_width(self):
        size = self.select_pen_size_spinbox.value()
        self.set_pen_width('select', size)

    def set_include_pen_width(self):
        size = self.include_pen_size_spinbox.value()
        self.set_pen_width('include', size)

    def set_exclude_pen_width(self):
        size = self.exclude_pen_size_spinbox.value()
        self.set_pen_width('exclude', size)

    def set_pen_width(self, mode, size):
        self.graphics_scene.set_pen_width(mode, size)

    def refresh_channels(self):
        button = self.colorspace_selection_group.checkedButton()
        self.current_colorspace = button.text()
        # clean channels list
        empty_layout(self.channel_layout)
        if self.current_colorspace == 'Original':
            color_bundle = self.data.reference
        else:
            current_processor = active_tab_title(self.colorspace_container_widget)
            color_bundle = self.data.processed[current_processor][button.text()]
        # populate group with selected channels
        composite = ColorVariant(label='Composite', thumbnail=color_bundle.composite)
        self.channel_layout.addWidget(composite)
        self.channel_selection_group.addButton(composite)
        composite.click()
        for index, channel in enumerate(color_bundle.channels):
            item = ColorVariant(label=f'Channel {index + 1}', thumbnail=color_bundle.channels[index])
            self.channel_layout.addWidget(item)
            self.channel_selection_group.addButton(item)

    def snapshot(self):
        button = self.colorspace_selection_group.checkedButton()
        if button.text() == 'Original':
            color_bundle = self.data.reference
        else:
            current_processor = active_tab_title(self.colorspace_container_widget)
            colorspace = self.colorspace_selection_group.checkedButton().text()
            color_bundle = self.data.processed[current_processor][colorspace]
        label = self.channel_selection_group.checkedButton().text()
        color_bundle.get_stats()
        if label.startswith('Channel '):
            image = color_bundle.channels[int(label.split()[-1]) - 1]
        else:
            image = color_bundle.composite
        image_name, ext = os.path.splitext(os.path.basename(self.data.image_path))
        suffix = f'{color_bundle.code}-{label}'
        directory = QtWidgets.QFileDialog.getExistingDirectory(self, 'Choose destination folder', os.path.dirname(self.data.image_path), QtWidgets.QFileDialog.ShowDirsOnly | QtWidgets.QFileDialog.DontResolveSymlinks) 
        image.save(os.path.join(directory, f'{image_name}-{suffix}.tif'))

    def refresh_canvas(self):
        button = self.colorspace_selection_group.checkedButton()
        if button.text() == 'Original':
            color_bundle = self.data.reference
        else:
            current_processor = active_tab_title(self.colorspace_container_widget)
            colorspace = self.colorspace_selection_group.checkedButton().text()
            color_bundle = self.data.processed[current_processor][colorspace]
        label = self.channel_selection_group.checkedButton().text()
        if label.startswith('Channel '):
            image = color_bundle.channels[int(label.split()[-1]) - 1]
        else:
            image = color_bundle.composite
        pixmap = QtGui.QPixmap.fromImage(image)
        self.graphics_scene.set_background_pixmap(pixmap)

    def refresh_tabbed_colorspace_picker(self):
        current_page = self.colorspace_container_widget.currentIndex()
        if current_page != -1:
            tab_text = self.colorspace_container_widget.tabText(current_page)
        else:
            tab_text = None
        current_colorspace = self.colorspace_selection_group.checkedButton()
        if current_colorspace:
            colorspace_text = current_colorspace.text()
        else:
            colorspace_text = 'Original'
        self.colorspace_container_widget.clear()
        for button in self.colorspace_selection_group.buttons():
            self.colorspace_selection_group.removeButton(button)
        self.ref_thumbnail = ColorVariant(label='Original', thumbnail=self.data.reference.composite)
        self.colorspace_selection_group.addButton(self.ref_thumbnail)
        if colorspace_text == 'Original':
            active_colorspace = self.ref_thumbnail
        for processor in self.data.processed:
            colorspace_scroller = QtWidgets.QScrollArea()
            colorspace_widget = QtWidgets.QWidget(colorspace_scroller)
            colorspace_layout = QtWidgets.QGridLayout(colorspace_widget)
            colorspace_widget.setLayout(colorspace_layout)

            variations = len(color_interpreters)

            colorspace_coords = [(i//2, i%2) for i in range(1, variations + 1)]
            for colorspace, coords in zip(self.data.processed[processor], colorspace_coords):
                item = ColorVariant(label=colorspace, thumbnail=self.data.processed[processor][colorspace].composite)
                colorspace_layout.addWidget(item, *coords)
                self.colorspace_selection_group.addButton(item)
                if processor == tab_text or tab_text is None:
                    if colorspace == self.current_colorspace:
                        active_colorspace = item
            colorspace_scroller.setWidget(colorspace_widget)
            colorspace_scroller.setWidgetResizable(True)
            self.colorspace_container_widget.addTab(colorspace_scroller, processor)
        buttons = self.colorspace_selection_group.buttons()
        if current_page != -1:
            self.colorspace_container_widget.setCurrentIndex(current_page)
        active_colorspace.click()

    def load_data(self):
        self.setCentralWidget(self.tab_widget)
        self.reset_prediction_parameters()
        name = QtWidgets.QFileDialog.getOpenFileName(self, 'Open Image', filter="Image Files (*.png *.jpg *.bmp *.tif)")
        if self.graphics_scene:
            del self.graphics_scene
        self.graphics_scene = CustomScene()
        self.image_widget.setScene(self.graphics_scene)
        self.graphics_scene.view = self.image_widget
        self.reset_supervision_parameters()
        
        self.status.showMessage('Loading picture…')
        if name[0] != "":
            
            self.data.load(name[0], self.progress_bar)
            self.refresh_tabbed_colorspace_picker()
            self.progress_bar.reset()
            self.status.clearMessage()
            #self.image_widget.adjustSize()
            self.image_widget.init_view()

    def display_help(self):

        form = Help('index.html', self)
        form.show()
    
    def display_about(self):
        with open(about_content, 'r') as about_file:
            message = about_file.read()
        
        QtWidgets.QMessageBox.about(self, "General workflow", message)
    
    def modal_error(self, message):
        QtWidgets.QMessageBox.critical(self, "ERA - Error", message)
                

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    locale = QtCore.QLocale.system().name()
    print(locale)
    qtTranslator = QtCore.QTranslator()
    if qtTranslator.load("qt_" + locale, ":/"):
        app.installTranslator(qtTranslator)
    appTranslator = QtCore.QTranslator()
    if appTranslator.load("ERA_" + locale, "./"):
        app.installTranslator(appTranslator)
    window = MainWindow()
    window.showMaximized()
    window.showNormal()
    window.showMaximized()
    app.exec_()
