from PIL import Image
import cv2
import numpy

class ColorInterpreter:
    name = 'CMY(K)'
    order = 80

    def interpret(self, matrix):
        """
        return CMYK matrix from image.
        """
        data = Image.fromarray(matrix)
        converted = numpy.array(data.convert('CMYK'))
        x, y, z, k = cv2.split(converted)
        return numpy.dstack((x, y, z))
