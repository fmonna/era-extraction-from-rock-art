import numpy as np

def matrix_stats(matrix):
    w, h, c = matrix.shape
    stats = {}
    stats['min'] = np.min(matrix.T.reshape(c, h * w), axis=1)
    stats['max'] = np.max(matrix.T.reshape(c, h * w), axis=1)
    return stats
    
def whiten(X, x, method='zca'):
    """
    Whitens the input matrix X using specified whitening method.
    Inputs:
        X:      Input data matrix with data examples along the first dimension
        method: Whitening method. Must be one of 'zca', 'zca_cor', 'pca',
                'pca_cor', or 'cholesky'.
    """
    x = x.reshape((-1, np.prod(x.shape[1:])))
    X = X.reshape((-1, np.prod(X.shape[1:])))
    x_centered = x - np.mean(x, axis=0)
    X_centered = X - np.mean(x, axis=0)
    Sigma = np.dot(x_centered.T, x_centered) / x_centered.shape[0]
    W = None
 
    if method in ['zca', 'pca', 'cholesky']:
        U, Lambda, _ = np.linalg.svd(Sigma)
        if method == 'zca':
            W = np.dot(U, np.dot(np.diag(1.0 / np.sqrt(Lambda + 1e-5)), U.T))
        elif method =='pca':
            W = np.dot(np.diag(1.0 / np.sqrt(Lambda + 1e-5)), U.T)
        elif method == 'cholesky':
            W = np.linalg.cholesky(np.dot(U, np.dot(np.diag(1.0 / (Lambda + 1e-5)), U.T))).T
    elif method in ['zca_cor', 'pca_cor']:
        V_sqrt = np.diag(np.std(X, axis=0))
        P = np.dot(np.dot(np.linalg.inv(V_sqrt), Sigma), np.linalg.inv(V_sqrt))
        G, Theta, _ = np.linalg.svd(P)
        if method == 'zca_cor':
            W = np.dot(np.dot(G, np.dot(np.diag(1.0 / np.sqrt(Theta + 1e-5)), G.T)), np.linalg.inv(V_sqrt))
        elif method == 'pca_cor':
            W = np.dot(np.dot(np.diag(1.0/np.sqrt(Theta + 1e-5)), G.T), np.linalg.inv(V_sqrt))
    else:
        raise Exception('Whitening method not found.')

    b = np.dot(x_centered, W.T)
    a = np.dot(X_centered, W.T)
    return (a,b)


def new_range(a, subset, keep):
 
    gmax = np.quantile(subset[:,0], 1-(keep/200))
    gmin = np.quantile(subset[:,0],   (keep/200))


    a[:,0][a[:,0] >= gmax] = gmax
    a[:,0][a[:,0] <= gmin] = gmin

    gmax = np.quantile(subset[:,1], 1-(keep/200))
    gmin = np.quantile(subset[:,1],   (keep/200))

    a[:,1][a[:,1] >= gmax] = gmax
    a[:,1][a[:,1] <= gmin] = gmin

    gmax = np.quantile(subset[:,2], 1-(keep/200))
    gmin = np.quantile(subset[:,2],   (keep/200))

    a[:,2][a[:,2] >= gmax] = gmax
    a[:,2][a[:,2] <= gmin] = gmin

    a[:,0] = 255 * (a[:,0]-np.min(a[:,0])) / (np.max(a[:,0])-np.min(a[:,0]))
    a[:,1] = 255 * (a[:,1]-np.min(a[:,1])) / (np.max(a[:,1])-np.min(a[:,1]))
    a[:,2] = 255 * (a[:,2]-np.min(a[:,2])) / (np.max(a[:,2])-np.min(a[:,2]))

    out = a.astype(np.uint8)
    return out


def reshape_scale(im_matrix, subset, w, h, keep):

    im_matrix = new_range(im_matrix, subset, keep)
    #print('im_matrix shape')
    #print(im_matrix.shape)
    x = im_matrix[:,0].reshape((w, h))
    y = im_matrix[:,1].reshape((w, h))
    z = im_matrix[:,2].reshape((w, h))

    return np.dstack((x, y, z))
