import cv2

class ColorInterpreter:
    name = 'HSV'
    order = 70

    def interpret(self, matrix):
        """
        return HSV matrix from image.
        """
        converted = cv2.cvtColor(matrix, cv2.COLOR_RGB2HSV)
        return converted
