from qt import QtWidgets, QtGui, QtCore

class Help(QtWidgets.QDialog):

    def __init__(self, page, parent=None):
        super(Help, self).__init__(parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setAttribute(QtCore.Qt.WA_GroupLeader)

        self.pageLabel = QtWidgets.QLabel()

        self.textBrowser = QtWidgets.QTextBrowser()

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.textBrowser)
        self.setLayout(layout)

        self.textBrowser.setSearchPaths([":/"])
        self.textBrowser.setSource(QtCore.QUrl(page))
        self.resize(400, 600)
        self.setWindowTitle(self.tr("Help"))

class ColorVariant(QtWidgets.QAbstractButton):
    def __init__(self, label, thumbnail, parent=None):
        super(ColorVariant, self).__init__(parent=parent)
        self.setSizePolicy(
            QtWidgets.QSizePolicy.MinimumExpanding,
            QtWidgets.QSizePolicy.MinimumExpanding
        )
        self.setText(label)
        self.full_res = thumbnail
        self.thumbnail_proportion = self.full_res.size().height()/self.full_res.size().width()
        self.setCheckable(True)
        self.setChecked(False)

    def sizeHint(self):
        return QtCore.QSize(150, 150)

    def paintEvent(self, event):
        #
        # |—widget————————————————|
        # ||——frame——————————————||
        # || label               ||
        # |||——————thumbnail box|||
        # |||m   margin         |||
        # |||a  |—————————————| |||
        # |||r  |  thumbnail  | |||
        # |||g  |             | |||
        # |||i  |—————————————| |||
        # |||n                  |||
        # |||———————————————————|||
        # ||—————————————————————||
        # |———————————————————————|
        #
        widget_height = self.size().height()
        widget_width = self.size().width()
        frame_offset = 1
        label_height = 17
        thumbnail_box_height = widget_height - 2 * frame_offset - label_height
        thumbnail_box_width = widget_width - 2 * frame_offset
        thumbnail_box_proportion = thumbnail_box_height/thumbnail_box_width

        if self.thumbnail_proportion > thumbnail_box_proportion:
            thumbnail = self.full_res.scaledToHeight(thumbnail_box_height)
        else:
            thumbnail = self.full_res.scaledToWidth(thumbnail_box_width)

        vertical_margin = (thumbnail_box_height - thumbnail.size().height()) // 2
        horizontal_margin = (thumbnail_box_width - thumbnail.size().width()) // 2
        thumbnail_vertical_anchor =  frame_offset + label_height + vertical_margin
        thumbnail_horizontal_anchor = frame_offset + horizontal_margin 

        painter = QtGui.QPainter(self)
        # frame
        painter.drawRect(frame_offset, frame_offset, self.size().width() - 2 * frame_offset, self.size().height() - 2 * frame_offset)
        # thumbnail
        painter.drawImage(thumbnail_horizontal_anchor, thumbnail_vertical_anchor, thumbnail)
        # label
        radio_pos = QtCore.QPoint(10, 10)
        painter.drawEllipse(radio_pos, 5, 5)
        font = QtGui.QFont()
        font.setPointSize(10)
        painter.setFont(font)
        painter.drawText(18, 15, self.text())
        if self.isChecked():
            brush = QtGui.QBrush(QtCore.Qt.black)
            painter.setBrush(brush)
            painter.drawEllipse(radio_pos, 3, 3)


class ImageWidget(QtWidgets.QGraphicsView):
    def __init__(self, parent=None, bounding_box=None):
        super(ImageWidget, self).__init__(parent=parent)
        self._zoom = 0
        self.bounding_box = bounding_box or QtCore.QRectF(0., 0., 200., 200.)
        self.prop = 1
        self.center = QtCore.QPointF(100., 100.)
        #self.fitInView(self.bounding_box, aspectRatioMode=QtCore.Qt.KeepAspectRatio)
        #self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        #self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.current_pen_size = 10

    def set_pen_size(self, size):
        self.current_pen_size = size

    def update_prop(self):
        self.prop = self.bounding_box.width()/self.bounding_box.height()

    def resizeEvent(self, event):
        self.update_center(event)
        return super(ImageWidget, self).resizeEvent(event)

    def update_center(self, event):
        delta_x = event.size().width() - event.oldSize().width()
        delta_y = event.size().height() - event.oldSize().height()
        #scale dependant factor
        zoomInFactor = 1.25
        m11 = self.transform().m11()
        scale_factor = 1/(zoomInFactor * m11)
        self.translate(delta_x * scale_factor, delta_y * scale_factor)

    def init_view(self, size=None):
        # Set Anchors
        if size:
            height = size.height()
            width = size.width()
        else:
            height = self.size().height()
            width = self.size().width()
        self.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
        self.setResizeAnchor(QtWidgets.QGraphicsView.NoAnchor)
        delta_prop = self.size().width()/self.size().height() - self.prop
        if delta_prop >= 0:
            scale = (height-2)/self.bounding_box.height()
        else:
            scale = (width-2)/self.bounding_box.width()
        self.centerOn(self.bounding_box.width()/2, self.bounding_box.height())
        self.setTransform(self.transform().fromScale(scale, scale))

    def wheelEvent(self, event):
        # Zoom Factor
        zoomInFactor = 1.25
        zoomOutFactor = 1 / zoomInFactor
        m11 = self.transform().m11()

        # Set Anchors
        self.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
        self.setResizeAnchor(QtWidgets.QGraphicsView.NoAnchor)

        # Save the scene pos
        oldPos = self.mapToScene(event.pos())

        # Zoom
        if event.angleDelta().y() > 0:
            zoomFactor = zoomInFactor * m11
            self._zoom += 1
        else:
            zoomFactor = zoomOutFactor * m11
            self._zoom -= 1
        if self._zoom > 0:
            self.setTransform(self.transform().fromScale(zoomFactor, zoomFactor))
        elif self._zoom == 0:
            self.init_view()
        else:
            self._zoom = 0

        # Get the new position
        newPos = self.mapToScene(event.pos())
        self.center = newPos

        # Move scene to old position
        delta = newPos - oldPos
        self.translate(delta.x(), delta.y())

    def history_event(self, event):
        if event == 'redo':
            self.scene().redo()
        elif event == 'undo':
            self.scene().undo()

    def keyPressEvent(self, event):
        if event.key() == 82:
            self.scene().redo('include')
        elif event.key() == 85:
            self.scene().undo('include')
        else:
            print(event.key())

    def mousePressEvent(self, event):
        current_point = self.mapToScene(event.pos()).toPoint()
        if event.button() == QtCore.Qt.LeftButton:
            self.scene().start_stroke(current_point)

    def mouseReleaseEvent(self, event):
        current_point = self.mapToScene(event.pos()).toPoint()
        if event.button() == QtCore.Qt.LeftButton:
            self.scene().end_stroke(current_point)

    def mouseMoveEvent(self, event):
        current_point = self.mapToScene(event.pos()).toPoint()
        self.scene().draw_stroke(current_point)


class CustomScene(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super(CustomScene, self).__init__(parent=parent)
        self.background_pixmap = None
        self.view = None
        self.drawing = False
        self.current_stroke = None
        self.last_point = None
        self.history = {'include': {'all': [], 'length': 0},
                        'exclude': {'all': [], 'length': 0},
                        'select': {'all': [], 'length': 0},
                        }
        self.masked = QtWidgets.QGraphicsItemGroup()
        self.addItem(self.masked)
        self.masked.setVisible(False)
        self.layers = {'select': QtWidgets.QGraphicsItemGroup(),
                       'include': QtWidgets.QGraphicsItemGroup(),
                       'exclude': QtWidgets.QGraphicsItemGroup(),
                       }
        self.pens = {'include': QtGui.QPen(QtCore.Qt.black, 10, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin),
                     'exclude': QtGui.QPen(QtCore.Qt.red, 10, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin),
                     'select': QtGui.QPen(QtCore.Qt.yellow, 10, QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin),
                     }
        self.current_pen_mode = 'select'
        self.active_pen = None

    def set_pen_mode(self, mode):
        self.current_pen_mode = mode
        for l in self.layers.values():
            l.setVisible(False)
        if mode == 'select':
            self.layers['select'].setVisible(True)
        else:
            for l in ['include', 'exclude']:
                self.layers[l].setVisible(True)

    def undo(self):
        layer = self.current_pen_mode
        if self.history[layer]['all'] and self.history[layer]['length'] > 0:
            item = self.history[layer]['all'][self.history[layer]['length'] - 1]
            self.masked.addToGroup(item)
            self.history[layer]['length'] -= 1
            self.update()

    def redo(self):
        layer = self.current_pen_mode
        if self.history[layer]['all'] and self.history[layer]['length'] < len(self.history[layer]['all']):
            item = self.history[layer]['all'][self.history[layer]['length']]
            self.layers[layer].addToGroup(item)
            self.history[layer]['length'] += 1

    def flush_history(self, length=None, layer=None):
        if layer is None:
            layer = self.current_pen_mode
        if length is None:
            length = self.history[layer]['length']
        while len(self.history[layer]['all']) > length:
            item = self.history[layer]['all'].pop()
            if item in self.layers[layer].childItems():
                self.layers[layer].removeFromGroup(item)
            elif item in self.masked.childItems():
                self.masked.removeFromGroup(item)
            del item
        self.history[layer]['length'] = len(self.history[layer]['all'])

    def flush_all_history(self):
        for layer in self.layers:
            self.flush_history(length=0, layer=layer)

    def set_pen(self, pen, layer):
        self.pens[layer] = pen

    def increment_pen(self, layer):
        self.pens[layer].setWidth(self.pens[layer].width() + 1)

    def decrement_pen(self, layer):
        self.pens[layer].setWidth(self.pens[layer].width() - 1)

    def set_pen_width(self, layer, width):
        self.pens[layer].setWidth(width)

    def sanitized_pos(self, pos):
        if not self.background_pixmap.contains(pos):
            if pos.x() < 0:
                pos.setX(0.)
            elif pos.x() > self.background_pixmap.boundingRect().width():
                pos.setX(self.background_pixmap.boundingRect().width())
            if pos.y() < 0:
                pos.setY(0.)
            elif pos.y() > self.background_pixmap.boundingRect().height():
                pos.setY(self.background_pixmap.boundingRect().height())
        return pos

    def start_stroke(self, pos):
        self.drawing = True
        self.flush_history()
        self.current_stroke = QtWidgets.QGraphicsItemGroup()
        self.addItem(self.current_stroke)
        if self.last_point is None:
            self.last_point = self.sanitized_pos(pos)

    def draw_stroke(self, pos):
        layer = self.current_pen_mode
        active_pen = self.pens[layer]
        if self.drawing and self.last_point:
            pos = self.sanitized_pos(pos)
            line = QtCore.QLineF(self.last_point, pos)
            if line.length() > 3:
                line_item = QtWidgets.QGraphicsLineItem()
                line_item.setPen(active_pen)
                line_item.setLine(line)
                self.current_stroke.addToGroup(line_item)
                self.last_point = pos

    def end_stroke(self, pos):
        layer = self.current_pen_mode
        self.drawing = False
        self.history[layer]['all'].append(self.current_stroke)
        self.layers[layer].addToGroup(self.current_stroke)
        self.last_point = None
        self.current_stroke = None
        self.history[layer]['length'] = len(self.history[layer]['all'])

    def set_background_pixmap(self, pixmap):
        if not self.background_pixmap:
            self.background_pixmap = QtWidgets.QGraphicsPixmapItem()
            self.addItem(self.background_pixmap)
        self.view.bounding_box = QtCore.QRectF(0., 0., float(pixmap.size().width()), float(pixmap.size().height()))
        self.view.update_prop()
        self.background_pixmap.setPixmap(pixmap)
        for value in self.layers.values():
            self.addItem(value)

    def render_layer(self, layer):
        image = QtGui.QImage(self.background_pixmap.boundingRect().toRect().size(), QtGui.QImage.Format_RGB32)
        image.fill(QtCore.Qt.white)
        painter = QtGui.QPainter(image)
        self.background_pixmap.setVisible(False)
        visible_layers = [l for l in self.layers.values() if l.isVisible()]
        hidden_layers = [l for l in self.layers.values() if not l.isVisible()]
        for l in visible_layers:
            l.setVisible(False)
        self.layers[layer].setVisible(True)
        self.render(painter, self.background_pixmap.boundingRect(), self.background_pixmap.boundingRect())
        painter.end()
        self.background_pixmap.setVisible(True)
        for l in visible_layers:
            l.setVisible(True)
        for l in hidden_layers:
            l.setVisible(False)
        image.save(f'{layer}.png')
        return image



class ResultView(QtWidgets.QGraphicsView):
    def __init__(self, parent=None, bounding_box=None):
        super(ResultView, self).__init__(parent=parent)
        self._zoom = 0
        self.bounding_box = bounding_box or QtCore.QRectF(0., 0., 200., 200.)
        self.prop = 1
        self.center = QtCore.QPointF(100., 100.)

    def update_prop(self):
        self.prop = self.bounding_box.width()/self.bounding_box.height()

    def resizeEvent(self, event):
        self.update_center(event)
        return super(ResultView, self).resizeEvent(event)

    def update_center(self, event):
        delta_x = event.size().width() - event.oldSize().width()
        delta_y = event.size().height() - event.oldSize().height()
        #scale dependant factor
        zoomInFactor = 1.25
        m11 = self.transform().m11()
        scale_factor = 1/(zoomInFactor * m11)
        self.translate(delta_x * scale_factor, delta_y * scale_factor)

    def init_view(self):
        # Set Anchors
        self.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
        self.setResizeAnchor(QtWidgets.QGraphicsView.NoAnchor)
        delta_prop = self.size().width()/self.size().height() - self.prop
        if delta_prop >= 0:
            scale = (self.size().height()+5)/self.bounding_box.height()
        else:
            scale = (self.size().width()+5)/self.bounding_box.width()
        self.centerOn(self.bounding_box.width()/2, self.bounding_box.height())
        self.setTransform(self.transform().fromScale(scale, scale))

    def wheelEvent(self, event, propagate=True):
        # Zoom Factor
        zoomInFactor = 1.25
        zoomOutFactor = 1 / zoomInFactor
        m11 = self.transform().m11()

        # Set Anchors
        self.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
        self.setResizeAnchor(QtWidgets.QGraphicsView.NoAnchor)

        # Save the scene pos
        oldPos = self.mapToScene(event.pos())

        # Zoom
        if event.angleDelta().y() > 0:
            zoomFactor = zoomInFactor * m11
            self._zoom += 1
        else:
            zoomFactor = zoomOutFactor * m11
            self._zoom -= 1
        if self._zoom > 0:
            self.setTransform(self.transform().fromScale(zoomFactor, zoomFactor))
        elif self._zoom == 0:
            self.init_view()
        else:
            self._zoom = 0

        # Get the new position
        newPos = self.mapToScene(event.pos())
        self.center = newPos

        # Move scene to old position
        delta = newPos - oldPos
        self.translate(delta.x(), delta.y())
        if propagate and self.parent():
            self.parent().propagate_zoom(event)


class ResultWidget(QtWidgets.QWidget):
    def __init__(self, label, parent=None):
        super(ResultWidget, self).__init__(parent=parent)
        layout = QtWidgets.QVBoxLayout()
        self.background_pixmap = None
        self.scene = QtWidgets.QGraphicsScene()
        self.view = ResultView()
        self.view.setScene(self.scene)
        self.checkbox = QtWidgets.QCheckBox(label)
        layout.addWidget(self.checkbox)
        layout.addWidget(self.view)
        self.setLayout(layout)

    def propagate_zoom(self, event):
        for child in self.parent().children():
            if not child is self and isinstance(child, ResultWidget):
                child.view.wheelEvent(event, propagate=False)

    def set_background_pixmap(self, pixmap):
        if not self.background_pixmap:
            self.background_pixmap = QtWidgets.QGraphicsPixmapItem()
            self.scene.addItem(self.background_pixmap)
        self.view.bounding_box = QtCore.QRectF(0., 0., float(pixmap.size().width()), float(pixmap.size().height()))
        self.view.update_prop()
        self.background_pixmap.setPixmap(pixmap)
        self.init_view()

    def is_selected_for_export(self):
        if self.checkbox.isChecked():
            return True
        return False

    def get_image(self):
        return self.background_pixmap.pixmap()

    def init_view(self):
        # Set Anchors
        self.view.setTransformationAnchor(QtWidgets.QGraphicsView.NoAnchor)
        self.view.setResizeAnchor(QtWidgets.QGraphicsView.NoAnchor)
        delta_prop = self.view.size().width()/self.view.size().height() - self.view.prop
        if delta_prop >= 0:
            scale = (self.view.size().height()+5)/self.view.bounding_box.height()
        else:
            scale = (self.view.size().width()+5)/self.view.bounding_box.width()
        self.view.centerOn(self.view.bounding_box.width()/2, self.view.bounding_box.height())
        self.view.setTransform(self.view.transform().fromScale(scale, scale))

    def uncheck(self):
        self.checkbox.setChecked(False)
