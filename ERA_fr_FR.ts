<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>Help</name>
    <message>
        <location filename="widgets.py" line="21"/>
        <source>Help</source>
        <translation type="unfinished">Aide</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="main.py" line="92"/>
        <source>Open</source>
        <translation type="unfinished">Ouvrir</translation>
    </message>
    <message>
        <location filename="main.py" line="94"/>
        <source>Open an image</source>
        <translation type="unfinished">Ouvrir une image</translation>
    </message>
    <message>
        <location filename="main.py" line="97"/>
        <source>Exit</source>
        <translation type="unfinished">Quitter</translation>
    </message>
    <message>
        <location filename="main.py" line="99"/>
        <source>Close application</source>
        <translation type="unfinished">Fermer l’application</translation>
    </message>
    <message>
        <location filename="main.py" line="102"/>
        <source>Snapshot</source>
        <translation type="unfinished">Capture</translation>
    </message>
    <message>
        <location filename="main.py" line="104"/>
        <source>Capture the main window</source>
        <translation type="unfinished">Capturer la fenêtre principale</translation>
    </message>
    <message>
        <location filename="main.py" line="108"/>
        <source>Help</source>
        <translation type="unfinished">Aide</translation>
    </message>
    <message>
        <location filename="main.py" line="110"/>
        <source>General workflow described</source>
        <translation type="unfinished">Description de la procédure</translation>
    </message>
    <message>
        <location filename="main.py" line="113"/>
        <source>About...</source>
        <translation type="unfinished">À propos…</translation>
    </message>
    <message>
        <location filename="main.py" line="114"/>
        <source>Contributions</source>
        <translation type="unfinished">Contributions</translation>
    </message>
    <message>
        <location filename="main.py" line="119"/>
        <source>File</source>
        <translation type="unfinished">Fichier</translation>
    </message>
    <message>
        <location filename="main.py" line="120"/>
        <source>Capture</source>
        <translation type="unfinished">Capture</translation>
    </message>
    <message>
        <location filename="main.py" line="121"/>
        <source>?</source>
        <translation type="unfinished">?</translation>
    </message>
    <message>
        <location filename="main.py" line="133"/>
        <source>Standard ToolBar</source>
        <translation type="unfinished">Barre d’outils standard</translation>
    </message>
    <message>
        <location filename="main.py" line="140"/>
        <source>Ready to rock</source>
        <translation type="unfinished">Prêt</translation>
    </message>
    <message>
        <location filename="main.py" line="155"/>
        <source>Supervision</source>
        <translation type="unfinished">Supervision</translation>
    </message>
    <message>
        <location filename="main.py" line="206"/>
        <source>Undo last stroke for current mode</source>
        <translation type="unfinished">Annuler le dernier trait du mode courant</translation>
    </message>
    <message>
        <location filename="main.py" line="209"/>
        <source>Redo last stroke for current mode</source>
        <translation type="unfinished">Rétablir le dernier trait annulé du mode courant</translation>
    </message>
    <message>
        <location filename="main.py" line="213"/>
        <source>Reset current mode</source>
        <translation type="unfinished">Effacer l’historique des traits</translation>
    </message>
    <message>
        <location filename="main.py" line="218"/>
        <source>Continue</source>
        <translation type="unfinished">Poursuivre</translation>
    </message>
    <message>
        <location filename="main.py" line="249"/>
        <source>Stroke Width</source>
        <translation type="unfinished">Épaisseur de trait</translation>
    </message>
    <message>
        <location filename="main.py" line="252"/>
        <source>Contrast Boost</source>
        <translation type="unfinished">Gain de contraste</translation>
    </message>
    <message>
        <location filename="main.py" line="253"/>
        <source>Decorrelation Refinement</source>
        <translation type="unfinished">Affinement de la décorrélation</translation>
    </message>
    <message>
        <location filename="main.py" line="266"/>
        <source>Drawing</source>
        <translation type="unfinished">Dessin</translation>
    </message>
    <message>
        <location filename="main.py" line="304"/>
        <source>Blur Radius</source>
        <translation type="unfinished">Rayon de flou</translation>
    </message>
    <message>
        <location filename="main.py" line="310"/>
        <source>PCA var. explained:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.py" line="316"/>
        <source>n best channels:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.py" line="327"/>
        <source>Re-process</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.py" line="329"/>
        <source>Reprocess &lt;i&gt;Calculate&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.py" line="332"/>
        <source>Save</source>
        <translation type="unfinished">Sauver</translation>
    </message>
    <message>
        <location filename="main.py" line="334"/>
        <source>Save the checked images</source>
        <translation type="unfinished">Sauver les images sélectionnées</translation>
    </message>
    <message>
        <location filename="main.py" line="390"/>
        <source>Running prediction with {} method</source>
        <translation type="unfinished">Exécution de la prédection avec la méthode {}</translation>
    </message>
    <message>
        <location filename="main.py" line="383"/>
        <source>You must supervise first!

Choose the size of the pen and select the option include / exclude
for painting the areas corresponding to the figure and the surrounding, respectively.</source>
        <translation type="unfinished">Vous devez effectuer la supervision d’abord !

Choisissez la taille du trait et sélectionnez les options include / exclude
pour peindre les zones correpsondant à la figure et au fond, respectivement.</translation>
    </message>
    <message>
        <location filename="main.py" line="544"/>
        <source>Please select image to save first</source>
        <translation type="unfinished">Sélectionnez les images à sauvegarder d’abord</translation>
    </message>
    <message>
        <location filename="main.py" line="212"/>
        <source>Reset</source>
        <translation type="unfinished">Remettre à zéro</translation>
    </message>
    <message>
        <location filename="main.py" line="215"/>
        <source>Reset All</source>
        <translation type="unfinished">Tout remettre à zéro</translation>
    </message>
    <message>
        <location filename="main.py" line="216"/>
        <source>Reset all modes</source>
        <translation type="unfinished">Remettre à zéro les tracés pour tous les modes</translation>
    </message>
</context>
</TS>
