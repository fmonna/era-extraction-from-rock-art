import cv2

class ColorInterpreter:
    name = 'XYZ'
    order = 10

    def interpret(self, matrix):
        """
        return XYZ matrix from image.
        """
        converted = cv2.cvtColor(matrix, cv2.COLOR_RGB2XYZ)
        return converted
