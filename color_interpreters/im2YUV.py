import cv2

class ColorInterpreter:
    name = 'YUV'
    order = 50

    def interpret(self, matrix):
        """
        return YUV matrix from image.
        """
        converted = cv2.cvtColor(matrix, cv2.COLOR_RGB2YUV)
        return converted
